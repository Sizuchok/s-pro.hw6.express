import { Request, Response } from 'express'
import * as groupService from './groups.service'
import { ValidatedRequest } from 'express-joi-validation'
import IGroupCreateRequest from './types/group-create-request.interface'
import { IGroupUpdateRequest } from './types/group-update-request.interface'
import StatusCode from 'status-code-enum'

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupService.getAllGroups()
  response.json(groups)
}

export const getGroupById = async (request: Request<{ id: number }>, response: Response) => {
  const { id } = request.params
  const group = await groupService.getGroupById(id)
  response.json(group)
}

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupService.createGroup(request.body)
  response.status(StatusCode.SuccessCreated).json(group)
}

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params
  const result = await groupService.updateGroupById(id, request.body)
  response.json(result)
}

export const deleteGroupById = async (request: Request<{ id: number }>, response: Response) => {
  const { id } = request.params
  const result = await groupService.deleteGroupById(id)
  response.json(result)
}
