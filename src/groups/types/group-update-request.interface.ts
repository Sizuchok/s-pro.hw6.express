import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { IGroup } from './groups.interface'

export interface IGroupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IGroup>
}
