import Joi from 'joi'
import { IGroup } from './types/groups.interface'

export const updateGroupSchema = Joi.object<Partial<IGroup>>({
  name: Joi.string().min(3).max(16).optional(),
})

export const createGroupSchema = Joi.object<Omit<IGroup, 'id'>>({
  name: Joi.string().min(3).max(16).required(),
})
