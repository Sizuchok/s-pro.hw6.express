import StatusCode from 'status-code-enum'
import HttpError from '../application/exceptions/HttpError'
import { AppDataSource } from '../configs/database/data-source'
import { Group } from './entities/group.entity'
import { IGroup } from './types/groups.interface'
import { UpdateResult, DeleteResult } from 'typeorm'

const groupsRepository = AppDataSource.getRepository(Group)

export const getAllGroups = async (): Promise<IGroup[]> => {
  //could've use find() here as well
  return groupsRepository
    .createQueryBuilder('group')
    .select()
    .leftJoin('group.students', 'students')
    .addSelect('students')
    .getMany()
}

export const getGroupById = async (id: number): Promise<IGroup> => {
  const group = await groupsRepository.findOne({
    where: {
      id,
    },
    relations: {
      students: true,
    },
  })

  if (!group) throw new HttpError(StatusCode.ClientErrorNotFound, 'Student not found.')
  return group
}

export const createGroup = async (newGroup: Omit<IGroup, 'id'>): Promise<IGroup> => {
  return groupsRepository.save(newGroup)
}

export const updateGroupById = async (
  id: number,
  newGroupData: Partial<IGroup>,
): Promise<UpdateResult> => {
  const result = await groupsRepository.update(id, newGroupData)

  if (!result.affected)
    throw new HttpError(StatusCode.ClientErrorNotFound, 'Group not found for update.')

  return result
}

export const deleteGroupById = async (id: number): Promise<DeleteResult> => {
  const result = await groupsRepository.delete(id)

  if (!result.affected)
    throw new HttpError(StatusCode.ClientErrorNotFound, 'Group not found for deletion.')

  return result
}
