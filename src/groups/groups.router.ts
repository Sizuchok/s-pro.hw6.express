import controllerWrapper from '../application/utils/controllerWrapper'
import * as groupsController from './group.controller'
import { Router } from 'express'
import validator from '../application/middleware/validator.middleware'
import { idParamSchema } from '../application/schemas/id-param.schema'
import { createGroupSchema, updateGroupSchema } from './groups.schema'
import auth from '../application/middleware/basic-auth.middleware'

const router = Router()

router.get('/', controllerWrapper(groupsController.getAllGroups))
router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupById),
)
router.post('/', validator.body(createGroupSchema), controllerWrapper(groupsController.createGroup))
router.patch(
  '/:id',
  auth,
  validator.params(idParamSchema),
  validator.body(updateGroupSchema),
  controllerWrapper(groupsController.updateGroupById),
)
router.delete(
  '/:id',
  auth,
  validator.params(idParamSchema),
  controllerWrapper(groupsController.deleteGroupById),
)

export default router
