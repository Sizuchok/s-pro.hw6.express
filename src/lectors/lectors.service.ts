import StatusCode from 'status-code-enum'
import HttpError from '../application/exceptions/HttpError'
import { AppDataSource } from '../configs/database/data-source'
import { Course } from '../courses/entities/course.entity'
import { Lector } from './entities/lector.entity'
import { ILector } from './types/lectors.interface'
import { LectorCourse } from '../lectors_courses/entities/lector-course.entity'

const lectorsRepository = AppDataSource.getRepository(Lector)
const entityManager = AppDataSource.manager

export const createLector = async (newLectorData: Omit<ILector, 'id'>): Promise<ILector> => {
  return lectorsRepository.save(newLectorData)
}

export const associateWithCourse = async (
  lectorId: number,
  courseId: number,
): Promise<LectorCourse> => {
  const lector = await entityManager.findOneBy(Lector, { id: lectorId })

  if (!lector)
    throw new HttpError(
      StatusCode.ClientErrorNotFound,
      `Lector with { id: ${lectorId} } not found.`,
    )

  const course = await entityManager.findOneBy(Course, { id: courseId })

  if (!course)
    throw new HttpError(
      StatusCode.ClientErrorNotFound,
      `Course with { id: ${courseId} } not found.`,
    )

  const lectorCourse = new LectorCourse()
  lectorCourse.course = course
  lectorCourse.lector = lector

  return lectorCourse.save()
}

export const getAllLectors = async (): Promise<ILector[]> => {
  return lectorsRepository.find()
}

export const getLectorById = async (id: number): Promise<ILector> => {
  const lector = await lectorsRepository.findOne({
    where: { id },
    relations: {
      lectorCourses: {
        course: true,
      },
    },
  })
  if (!lector)
    throw new HttpError(StatusCode.ClientErrorNotFound, `Lector with { id: ${id} } not found.`)

  return lector
}
