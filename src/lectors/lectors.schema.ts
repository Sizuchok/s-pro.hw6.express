import Joi from 'joi'
import { ILector } from './types/lectors.interface'
import { ILectorCourse } from '../lectors_courses/types/lector-course.interface'

export const createLectorSchema = Joi.object<Omit<ILector, 'id'>>({
  name: Joi.string().min(3).max(16).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(8).max(24).pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
})

export const associateWithCourseSchema = Joi.object<Pick<ILectorCourse, 'courseId'>>({
  courseId: Joi.number().required(),
})
