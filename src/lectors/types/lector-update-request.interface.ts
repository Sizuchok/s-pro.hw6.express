import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { ILectorCourse } from '../../lectors_courses/types/lector-course.interface'

export default interface ILectorAssocCourseRequest extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Pick<ILectorCourse, 'lectorId'>
  [ContainerTypes.Body]: Pick<ILectorCourse, 'courseId'>
}
