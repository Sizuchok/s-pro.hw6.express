import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { ILector } from './lectors.interface'

export default interface ILectorCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ILector, 'id'>
}
