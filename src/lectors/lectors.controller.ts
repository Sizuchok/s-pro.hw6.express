import { Request, Response } from 'express'
import * as lectorsService from './lectors.service'
import { ValidatedRequest } from 'express-joi-validation'
import ILectorCreateRequest from './types/lector-create-request.interface'
import StatusCode from 'status-code-enum'
import ILectorAssocCourseRequest from './types/lector-update-request.interface'

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body)
  response.status(StatusCode.SuccessCreated).json(lector)
}

export const associateWithCourse = async (
  request: ValidatedRequest<ILectorAssocCourseRequest>,
  response: Response,
) => {
  const { lectorId } = request.params
  const { courseId } = request.body

  const lector = await lectorsService.associateWithCourse(lectorId, courseId)
  response.status(StatusCode.SuccessCreated).json(lector)
}

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors()
  response.json(lectors)
}

export const getLectorById = async (request: Request<{ id: number }>, response: Response) => {
  const lector = await lectorsService.getLectorById(request.params.id)
  response.json(lector)
}
