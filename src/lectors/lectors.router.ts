import { Router } from 'express'
import validator from '../application/middleware/validator.middleware'
import { associateWithCourseSchema, createLectorSchema } from './lectors.schema'
import controllerWrapper from '../application/utils/controllerWrapper'
import * as lectorsController from './lectors.controller'
import { idParamSchema } from '../application/schemas/id-param.schema'
import { lectorIdParamSchema } from '../application/schemas/lector-id.schema'

const router = Router()

router.post(
  '/',
  validator.body(createLectorSchema),
  controllerWrapper(lectorsController.createLector),
)

router.post(
  '/:lectorId/courses',
  validator.params(lectorIdParamSchema),
  validator.body(associateWithCourseSchema),
  controllerWrapper(lectorsController.associateWithCourse),
)

router.get('/', controllerWrapper(lectorsController.getAllLectors))

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(lectorsController.getLectorById),
)
export default router
