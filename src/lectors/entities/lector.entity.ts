import { Column, Entity, ManyToMany, OneToMany } from 'typeorm'
import { CoreEntity } from '../../application/entities/core.entity'
import { Course } from '../../courses/entities/course.entity'
import { LectorCourse } from '../../lectors_courses/entities/lector-course.entity'

@Entity('lectors')
export class Lector extends CoreEntity {
  @Column({
    type: 'varchar',
  })
  name: string

  @Column({
    type: 'varchar',
    unique: true,
  })
  email: string

  @Column({
    type: 'varchar',
  })
  password: string

  @ManyToMany(() => Course, course => course.lectors)
  courses: Course[]

  @OneToMany(() => LectorCourse, lectorCourse => lectorCourse.lector)
  public lectorCourses: LectorCourse[]
}
