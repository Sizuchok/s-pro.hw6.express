import 'dotenv/config'
import app from './application/app'

const PORT = process.env.PORT ?? 3030

const startServer = async () => {
  app.listen(PORT, () => console.log(`Server launched on port: ${PORT}`))
}

startServer()
