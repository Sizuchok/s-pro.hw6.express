import { DataSourceOptions } from 'typeorm'
import path from 'path'

export const databaseConfiguration = (isMigrationsRun = true): DataSourceOptions => {
  const migrationsPath = path.join(__dirname, '../../', '**/migrations/*.{js,ts}')
  const entitiesPath = path.join(__dirname, '../../', '**/*.entity.{js,ts}')

  return {
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_DATABASE,
    synchronize: false,
    logging: true,
    entities: [entitiesPath],
    migrations: [migrationsPath],
    migrationsRun: isMigrationsRun,
  }
}
