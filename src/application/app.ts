import express from 'express'
import studentsRouter from '../students/students.router'
import groupsRouter from '../groups/groups.router'
import marksRouter from '../marks/marks.router'
import lectorsRouter from '../lectors/lectors.router'
import coursesRouter from '../courses/courses.router'
import { exceptionsFilter } from './middleware/exception.filter'
import path from 'path'
import cors from 'cors'
import { AppDataSource } from '../configs/database/data-source'

const app = express()

app.use(cors())
app.use(express.json())

AppDataSource.initialize()
  .then(() => console.log('Connected to DB!'))
  .catch(error => console.error('Error', error))

app.use('/api/v1/students', studentsRouter)
app.use('/api/v1/groups', groupsRouter)
app.use('/api/v1/marks', marksRouter)
app.use('/api/v1/lectors', lectorsRouter)
app.use('/api/v1/courses', coursesRouter)

const staticFilesPath = path.join(__dirname, '../', 'public')
app.use('/api/v1/public', express.static(staticFilesPath))

app.use(exceptionsFilter)
export default app
