import basicAuth from 'express-basic-auth'
import 'dotenv/config'

const password = process.env.HW6_PASSWORD as string
const username = process.env.HW6_USERNAME as string

const auth = basicAuth({
  users: { [username]: password },
})

export default auth
