import { NextFunction, Request, Response } from 'express'
import HttpError from '../exceptions/HttpError'
import { StatusCode } from 'status-code-enum'

export const exceptionsFilter = (
  error: HttpError,
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const status = error.status ?? StatusCode.ServerErrorInternal
  const message = error.message || 'Something went wrong on our side.'
  response.status(status).send({ status, message })
}
