import multer from 'multer'
import path from 'path'

const imageDestinition = path.join(__dirname, '../../', 'temp')

const multerConfig = multer.diskStorage({
  destination: imageDestinition,
  filename: (request, file, saveAs) => saveAs(null, file.originalname),
})

const uploadMiddleware = multer({ storage: multerConfig })

export default uploadMiddleware
