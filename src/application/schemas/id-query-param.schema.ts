import Joi from 'joi'

export const idQueryParamSchema = Joi.object({
  id: Joi.number().optional(),
  lector_id: Joi.number().optional(),
  student_id: Joi.number().optional(),
  course_id: Joi.number().optional(),
})
