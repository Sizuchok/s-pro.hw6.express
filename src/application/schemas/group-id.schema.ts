import Joi from 'joi'

export const groupIdSchema = Joi.object<{ groupId: number }>({
  groupId: Joi.number().required(),
})
