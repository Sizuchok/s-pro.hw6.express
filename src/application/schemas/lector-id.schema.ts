import Joi from 'joi'

export const lectorIdParamSchema = Joi.object<{ lectorId: number }>({
  lectorId: Joi.number().required(),
})
