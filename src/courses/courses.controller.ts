import { ValidatedRequest } from 'express-joi-validation'
import * as coursesService from './courses.service'
import { ICourseCreateRequest } from './types/courses-create-request.interface'
import { Response } from 'express'
import StatusCode from 'status-code-enum'
import { ICourseGetRequest } from './types/courses-get-request.interface'

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await coursesService.createCourse(request.body)
  response.status(StatusCode.SuccessCreated).json(course)
}

export const getCourses = async (
  request: ValidatedRequest<ICourseGetRequest>,
  response: Response,
) => {
  let courses
  const lectorId = request.query.lector_id

  if (lectorId) courses = await coursesService.getCoursesByLectorId(lectorId)
  else courses = await coursesService.getAllCourses()

  response.json(courses)
}
