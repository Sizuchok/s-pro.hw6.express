import StatusCode from 'status-code-enum'
import HttpError from '../application/exceptions/HttpError'
import { AppDataSource } from '../configs/database/data-source'
import { Lector } from '../lectors/entities/lector.entity'
import { Course } from './entities/course.entity'
import { ICourse } from './types/courses.interface'

const coursesRepository = AppDataSource.getRepository(Course)
const lectorsRepository = AppDataSource.getRepository(Lector)

export const createCourse = async (newCourseData: Omit<ICourse, 'id'>): Promise<ICourse> => {
  return coursesRepository.save(newCourseData)
}

export const getAllCourses = (): Promise<ICourse[]> => {
  return coursesRepository.find()
}

export const getCoursesByLectorId = async (id: number): Promise<ICourse[]> => {
  const lector = await lectorsRepository.findOneBy({ id })

  if (!lector)
    throw new HttpError(StatusCode.ClientErrorNotFound, `Lector with { id: ${id} } not found.`)

  const courses = coursesRepository.findBy({
    lectorCourses: {
      lector: {
        id,
      },
    },
  })

  return courses
}
