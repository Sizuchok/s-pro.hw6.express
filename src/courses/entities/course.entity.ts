import { Column, Entity, ManyToMany, OneToMany } from 'typeorm'
import { CoreEntity } from '../../application/entities/core.entity'
import { Lector } from '../../lectors/entities/lector.entity'
import { LectorCourse } from '../../lectors_courses/entities/lector-course.entity'

@Entity('courses')
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    unique: true,
  })
  name: string

  @Column({
    type: 'text',
  })
  description: string

  @Column({
    type: 'integer',
  })
  hours: number

  @ManyToMany(() => Lector, lector => lector.courses)
  lectors: Lector[]

  @OneToMany(() => LectorCourse, lectorCourse => lectorCourse.course)
  public lectorCourses: LectorCourse[]
}
