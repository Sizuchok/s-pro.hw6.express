import Joi from 'joi'
import { ICourse } from './types/courses.interface'

export const createCourseSchema = Joi.object<Omit<ICourse, 'id'>>({
  name: Joi.string().min(3).max(16).required(),
  description: Joi.string().required(),
  hours: Joi.number().min(30).max(70).required(),
})
