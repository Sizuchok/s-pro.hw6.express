import { Router } from 'express'
import * as coursesController from './courses.controller'
import validator from '../application/middleware/validator.middleware'
import { createCourseSchema } from './courses.schema'
import controllerWrapper from '../application/utils/controllerWrapper'
import { idQueryParamSchema } from '../application/schemas/id-query-param.schema'

const router = Router()

router.post(
  '/',
  validator.body(createCourseSchema),
  controllerWrapper(coursesController.createCourse),
)

router.get(
  '/',
  validator.query(idQueryParamSchema),
  controllerWrapper(coursesController.getCourses),
)

export default router
