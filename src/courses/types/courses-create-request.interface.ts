import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { ICourse } from './courses.interface'

export interface ICourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ICourse, 'id'>
}
