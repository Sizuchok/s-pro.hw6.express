import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation'

export interface ICourseGetRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: { lector_id: number }
}
