import { MigrationInterface, QueryRunner } from 'typeorm'

export class MakeEmailColumnUnique1688123918788 implements MigrationInterface {
  name = 'MakeEmailColumnUnique1688123918788'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "UQ_25985d58c714a4a427ced57507b" UNIQUE ("email")`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" DROP CONSTRAINT "UQ_25985d58c714a4a427ced57507b"`,
    )
  }
}
