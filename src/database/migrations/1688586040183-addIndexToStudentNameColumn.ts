import { MigrationInterface, QueryRunner } from 'typeorm'

export class AddIndexToStudentNameColumn1688586040183 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE INDEX "student-name-index" ON "students" ("name") `)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP INDEX "public"."student-name-index"`)
  }
}
