import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateGroupsTableAndAddStudentGroupRelations1688133763340
  implements MigrationInterface
{
  name = 'CreateGroupsTableAndAddStudentGroupRelations1688133763340'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "groups" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "name" character varying NOT NULL, CONSTRAINT "PK_659d1483316afb28afd3a90646e" PRIMARY KEY ("id"))`,
    )
    await queryRunner.query(`ALTER TABLE "students" ADD "group_id" integer`)
    await queryRunner.query(
      `ALTER TABLE "students" ADD CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3" FOREIGN KEY ("group_id") REFERENCES "groups"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" DROP CONSTRAINT "FK_b9f6fcd8a397ee5b503191dd7c3"`,
    )
    await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "group_id"`)
    await queryRunner.query(`DROP TABLE "groups"`)
  }
}
