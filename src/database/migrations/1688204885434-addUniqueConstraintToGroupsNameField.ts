import { MigrationInterface, QueryRunner } from 'typeorm'

export class AddUniqueConstraintToGroupsNameField1688204885434 implements MigrationInterface {
  name = 'AddUniqueConstraintToGroupsNameField1688204885434'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "groups" ADD CONSTRAINT "UQ_664ea405ae2a10c264d582ee563" UNIQUE ("name")`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "groups" DROP CONSTRAINT "UQ_664ea405ae2a10c264d582ee563"`)
  }
}
