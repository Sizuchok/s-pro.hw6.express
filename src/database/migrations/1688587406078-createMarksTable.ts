import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateMarksTable1688587406078 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "marks" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "mark" integer NOT NULL, "student_id" integer NOT NULL, "course_id" integer NOT NULL, "lector_id" integer NOT NULL, CONSTRAINT "UQ_d9b96371110f5b449c2fada769c" UNIQUE ("course_id", "student_id", "lector_id"), CONSTRAINT "PK_051deeb008f7449216d568872c6" PRIMARY KEY ("id"))`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "marks"`)
  }
}
