import { MigrationInterface, QueryRunner } from 'typeorm'

export class CreateLectorCoursTable1688586954121 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "lector_course" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lector_id" integer, "course_id" integer, CONSTRAINT "UQ_79cf0f064235769277ce94c75f7" UNIQUE ("lector_id", "course_id"), CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658" PRIMARY KEY ("id"))`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "lector_course"`)
  }
}
