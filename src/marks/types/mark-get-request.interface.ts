import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'

export interface IMarkGetRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: { student_id: number; course_id: number }
}
