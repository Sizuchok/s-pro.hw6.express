import { Router } from 'express'
import controllerWrapper from '../application/utils/controllerWrapper'
import * as marksController from './marks.controller'
import validator from '../application/middleware/validator.middleware'
import { createMarkSchema } from './marks.schema'
import { idQueryParamSchema } from '../application/schemas/id-query-param.schema'

const router = Router()

router.get('/', validator.params(idQueryParamSchema), controllerWrapper(marksController.getMarks))

router.post(
  '/',
  validator.body(createMarkSchema),
  controllerWrapper(marksController.createMarkForStudent),
)

export default router
