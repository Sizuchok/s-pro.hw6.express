import Joi from 'joi'
import { IMark } from './types/marks.interface'

export const createMarkSchema = Joi.object<Omit<IMark, 'id'>>({
  mark: Joi.number().min(60).max(100).required(),
  courseId: Joi.number().required(),
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
})
