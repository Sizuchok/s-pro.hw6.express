import { Response } from 'express'
import * as marksService from './marks.service'
import { ValidatedRequest } from 'express-joi-validation'
import { IMarkCreateRequest } from './types/mark-create-request.interface'
import StatusCode from 'status-code-enum'
import { IMarkGetRequest } from './types/mark-get-request.interface'

export const getMarks = async (request: ValidatedRequest<IMarkGetRequest>, response: Response) => {
  const { student_id, course_id } = request.query
  let marks

  if (student_id) marks = await marksService.getMarksForStudentById(student_id)
  else if (course_id) marks = await marksService.getMarksForCourseById(course_id)
  else marks = await marksService.getAllMarks()
  response.json(marks)
}

export const createMarkForStudent = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: Response,
) => {
  const mark = await marksService.createMarkForStudent(request.body)
  response.status(StatusCode.SuccessCreated).json(mark)
}
