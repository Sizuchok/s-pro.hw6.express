import { Entity, Column, JoinColumn, ManyToOne, Unique } from 'typeorm'
import { CoreEntity } from '../../application/entities/core.entity'
import { Course } from '../../courses/entities/course.entity'
import { Student } from '../../students/entities/student.entity'
import { Lector } from '../../lectors/entities/lector.entity'

@Entity('marks')
@Unique(['course', 'student', 'lector'])
export class Mark extends CoreEntity {
  @Column({
    type: 'integer',
  })
  mark: number

  @ManyToOne(() => Course)
  @JoinColumn({
    name: 'course_id',
  })
  course: Course

  @ManyToOne(() => Student)
  @JoinColumn({
    name: 'student_id',
  })
  student: Student

  @ManyToOne(() => Lector)
  @JoinColumn({
    name: 'lector_id',
  })
  lector: Lector

  @Column({ name: 'student_id' })
  studentId: number

  @Column({ name: 'course_id' })
  courseId: number

  @Column({ name: 'lector_id' })
  lectorId: number
}
