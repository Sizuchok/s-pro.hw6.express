import StatusCode from 'status-code-enum'
import HttpError from '../application/exceptions/HttpError'
import { AppDataSource } from '../configs/database/data-source'
import { Mark } from './entities/mark.entity'
import { Student } from '../students/entities/student.entity'
import { IMark } from './types/marks.interface'
import { Course } from '../courses/entities/course.entity'
import { Lector } from '../lectors/entities/lector.entity'

const marksRepository = AppDataSource.getRepository(Mark)
const entityManager = AppDataSource.manager

export const getAllMarks = (): Promise<IMark[]> => {
  return marksRepository.find()
}

export const getMarksForStudentById = async (id: number): Promise<IMark[]> => {
  const student = await entityManager.findOneBy(Student, { id })

  if (!student)
    throw new HttpError(StatusCode.ClientErrorNotFound, `Student with { id: ${id} } not found.`)

  const studentMarks = await marksRepository
    .createQueryBuilder('mark')
    .select(['mark.mark as mark', 'course.name'])
    .leftJoin('mark.course', 'course')
    .where('mark.student_id = :id', { id })
    .getRawMany()

  return studentMarks
}

export const createMarkForStudent = async (newMarkData: Omit<IMark, 'id'>): Promise<IMark> => {
  const student = await entityManager.findOneBy(Student, { id: newMarkData.studentId })

  if (!student)
    throw new HttpError(
      StatusCode.ClientErrorNotFound,
      `Student with { id: ${newMarkData.studentId} } not found.`,
    )

  const course = await entityManager.findOneBy(Course, { id: newMarkData.courseId })

  if (!course)
    throw new HttpError(
      StatusCode.ClientErrorNotFound,
      `Course with { id: ${newMarkData.courseId} } not found.`,
    )

  const lector = await entityManager.findOneBy(Lector, {
    id: newMarkData.lectorId,
    lectorCourses: {
      course: {
        id: newMarkData.courseId,
      },
    },
  })

  if (!lector)
    throw new HttpError(
      StatusCode.ClientErrorNotFound,
      `Lector { id: ${newMarkData.lectorId} } associated with ${course.name} course not found.`,
    )

  return marksRepository.save(newMarkData)
}

export const getMarksForCourseById = async (id: number): Promise<IMark[]> => {
  const course = await entityManager.findOneBy(Course, { id })

  if (!course)
    throw new HttpError(StatusCode.ClientErrorNotFound, `Course with { id: ${id} } not found`)

  const marks = await marksRepository
    .createQueryBuilder('mark')
    .select([
      'mark.mark as mark',
      'student.name as "studentName"',
      'course.name as "courseName"',
      'lector.name as "lectorName"',
    ])
    .leftJoin('mark.student', 'student')
    .leftJoin('mark.course', 'course')
    .leftJoin('mark.lector', 'lector')
    .where('mark.course_id = :id', { id })
    .getRawMany()

  return marks
}
