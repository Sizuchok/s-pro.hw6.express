import { Entity, JoinColumn, ManyToOne, Unique } from 'typeorm'
import { CoreEntity } from '../../application/entities/core.entity'
import { Lector } from '../../lectors/entities/lector.entity'
import { Course } from '../../courses/entities/course.entity'

@Entity('lector_course')
@Unique(['lector', 'course'])
export class LectorCourse extends CoreEntity {
  @ManyToOne(() => Lector, lector => lector.lectorCourses)
  @JoinColumn({
    name: 'lector_id',
  })
  lector: Lector

  @ManyToOne(() => Course, course => course.lectorCourses)
  @JoinColumn({
    name: 'course_id',
  })
  course: Course
}
