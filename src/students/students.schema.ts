import Joi from 'joi'
import { IStudent } from './types/students.interface'

export const updateStudentSchema = Joi.object<Partial<IStudent>>({
  name: Joi.string().min(3).max(16).optional(),
  surname: Joi.string().min(3).max(16).optional(),
  age: Joi.number().min(14).max(90).optional(),
  email: Joi.string().email().optional(),
  groupId: Joi.number().optional(),
})

export const createStudentSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().min(3).max(16).required(),
  surname: Joi.string().min(3).max(16).required(),
  age: Joi.number().min(14).max(90).required(),
  email: Joi.string().email().required(),
})
