import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { IStudent } from './students.interface'
export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>
}

export interface IStudentAssignToGroupRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<IStudent, 'groupId'>
}
