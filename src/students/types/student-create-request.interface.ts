import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { IStudent } from './students.interface'

export default interface IStudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IStudent, 'id'>
}
