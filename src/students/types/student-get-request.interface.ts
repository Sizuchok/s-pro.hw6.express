import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import { IStudent } from './students.interface'

export default interface IStudentGetRequest extends ValidatedRequestSchema {
  [ContainerTypes.Query]: Pick<IStudent, 'name'>
}
