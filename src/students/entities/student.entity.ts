import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm'
import { CoreEntity } from '../../application/entities/core.entity'
import { Group } from '../../groups/entities/group.entity'

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Index('student-name-index')
  @Column({
    type: 'varchar',
  })
  name: string

  @Column({
    type: 'varchar',
  })
  surname: string

  @Column({
    type: 'varchar',
    unique: true,
  })
  email: string

  @Column({
    type: 'integer',
  })
  age: number

  @Column({
    name: 'image_path',
    type: 'varchar',
    nullable: true,
  })
  imagePath: string

  @ManyToOne(() => Group, group => group.students, {
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({
    name: 'group_id',
  })
  group: Group

  @Column({
    name: 'group_id',
    type: 'integer',
    nullable: true,
  })
  groupId: number
}
