import * as studentService from './students.service'
import { Request, Response } from 'express'
import { ValidatedRequest } from 'express-joi-validation'
import {
  IStudentUpdateRequest,
  IStudentAssignToGroupRequest,
} from './types/student-update-request.interface'
import IStudentCreateRequest from './types/student-create-request.interface'
import StatusCode from 'status-code-enum'
import IStudentGetRequest from './types/student-get-request.interface'

export const getStudents = async (
  request: ValidatedRequest<IStudentGetRequest>,
  response: Response,
) => {
  let students
  const name = request.query.name
  if (name) {
    students = await studentService.getStudentsByName(name)
  } else students = await studentService.getAllStudents()

  response.json(students)
}

export const updateStudent = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params
  const student = await studentService.updateStudent(id, request.body)
  response.json(student)
}

export const addImage = async (
  request: Request<{ id: number }, { file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params
  const result = await studentService.addImage(id, request.file?.path)
  response.json(result)
}

export const getStudentById = async (request: Request<{ id: number }>, response: Response) => {
  const { id } = request.params
  const student = await studentService.getStudentById(id)
  response.json(student)
}

export const deleteStudentById = async (request: Request<{ id: number }>, response: Response) => {
  const { id } = request.params
  const result = await studentService.deleteStudentById(id)
  response.json(result)
}

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentService.createStudent(request.body)
  response.status(StatusCode.SuccessCreated).json(student)
}

export const assignToGroup = async (
  request: ValidatedRequest<IStudentAssignToGroupRequest>,
  response: Response,
) => {
  const { id } = request.params
  const { groupId } = request.body
  const result = await studentService.assignToGroup(id, groupId)
  response.json(result)
}
