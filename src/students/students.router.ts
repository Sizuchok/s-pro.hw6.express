import { Router } from 'express'
import * as studentsController from './students.controller'
import controllerWrapper from '../application/utils/controllerWrapper'
import validator from '../application/middleware/validator.middleware'
import { idParamSchema } from '../application/schemas/id-param.schema'
import { createStudentSchema, updateStudentSchema } from './students.schema'
import uploadMiddleware from '../application/middleware/upload.middleware'
import auth from '../application/middleware/basic-auth.middleware'
import { groupIdSchema } from '../application/schemas/group-id.schema'

const router = Router()

router.get('/', controllerWrapper(studentsController.getStudents))

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentById),
)

router.post(
  '/',
  validator.body(createStudentSchema),
  controllerWrapper(studentsController.createStudent),
)

router.patch(
  '/:id',
  auth,
  validator.params(idParamSchema),
  validator.body(updateStudentSchema),
  controllerWrapper(studentsController.updateStudent),
)

router.patch(
  '/:id/group',
  auth,
  validator.params(idParamSchema),
  validator.body(groupIdSchema),
  controllerWrapper(studentsController.assignToGroup),
)

router.patch(
  '/:id/image',
  auth,
  validator.params(idParamSchema),
  uploadMiddleware.single('file'),
  controllerWrapper(studentsController.addImage),
)

router.delete(
  '/:id',
  auth,
  validator.params(idParamSchema),
  controllerWrapper(studentsController.deleteStudentById),
)

export default router
