import StatusCode from 'status-code-enum'
import HttpError from '../application/exceptions/HttpError'
import { IStudent } from './types/students.interface'
import ObjectID from 'bson-objectid'
import path from 'path'
import fs from 'fs/promises'
import { AppDataSource } from '../configs/database/data-source'
import { Student } from './entities/student.entity'
import { DeleteResult, SelectQueryBuilder, UpdateResult } from 'typeorm'

const studentsRepository = AppDataSource.getRepository(Student)

const getStudentSelectQueryBuilder = async (): Promise<SelectQueryBuilder<Student>> => {
  return studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.image_path as imagePath',
      'student.group_id as groupId',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as groupName')
}

export const getAllStudents = async (): Promise<Student[]> => {
  const studentQuery = await getStudentSelectQueryBuilder()
  return studentQuery.getRawMany()
}

export const getStudentById = async (id: number): Promise<Student> => {
  const studentSelectQuery = await getStudentSelectQueryBuilder()

  const student = await studentSelectQuery.where('student.id = :id', { id }).getRawOne()

  if (!student) throw new HttpError(StatusCode.ClientErrorNotFound, 'Student not found.')
  return student
}

export const getStudentsByName = async (name: string): Promise<Student[]> => {
  return studentsRepository.findBy({ name })
}

export const updateStudent = async (
  id: number,
  updatedStudentsScheme: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, updatedStudentsScheme)
  if (!result.affected)
    throw new HttpError(StatusCode.ClientErrorNotFound, 'Student not found for update.')
  return result
}

export const addImage = async (id: number, filePath?: string) => {
  if (!filePath) throw new HttpError(StatusCode.ClientErrorBadRequest, 'File is not provided.')

  try {
    //if user is not found in db  we want to delete image file from temp folder
    const student = studentsRepository.findOneBy({ id })

    if (!student) throw new HttpError(StatusCode.ClientErrorNotFound, 'Student not found.')

    const imageId = ObjectID().toHexString()
    const imageExt = path.extname(filePath)

    if (!imageExt)
      throw new HttpError(
        StatusCode.ClientErrorBadRequest,
        'Provided file has no extention or name.',
      )

    const imageName = imageId + imageExt

    const studentsFolderName = 'students'
    const studentsFolderPath = path.join(__dirname, '../', 'public', studentsFolderName)

    const studentImagePath = `${studentsFolderName}/${imageName}`
    const newImagePath = path.join(studentsFolderPath, imageName)

    await fs.rename(filePath, newImagePath)

    return studentsRepository.update(id, { imagePath: studentImagePath })
  } catch (error) {
    await fs.unlink(filePath)
    throw error
  }
}

export const deleteStudentById = async (id: number): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id)
  if (!result.affected)
    throw new HttpError(StatusCode.ClientErrorNotFound, 'Student not found for deletion.')
  return result
}

export const createStudent = async (newStudent: Omit<IStudent, 'id'>): Promise<Student> => {
  return await studentsRepository.save(newStudent)
}

export const assignToGroup = async (studentId: number, groupId: number) => {
  return updateStudent(studentId, { groupId })
}
